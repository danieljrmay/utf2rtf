"""Tests the pyinilint.py module."""

import unittest

from utf2rtf.utf2rtf import rtf_replacement


class TestRtfReplacement(unittest.TestCase):
    """Test the rtf_replacement(utf_char) function."""

    def setup(self):
        """Setup function"""
        pass

    def test_plain(self):
        """Test stuff."""
        self.assertTrue(True)
        self.assertEqual(rtf_replacement("a"), "a")


if __name__ == '__main__':
    unittest.main()
