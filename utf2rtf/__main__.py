#!/usr/bin/python3

"""
Converts a UTF-8 input file to an output file where all characters
have been replaced (where required and possible) with RTF escape
sequences.
"""

import argparse

from utf2rtf.utf2rtf import rtf_replacement


def get_arg_parser():
    """Get the command line argument parser."""
    parser = argparse.ArgumentParser(
        description="Convert UTF-8 characters to RTF escape sequences."
    )

    parser.add_argument(
        "-o", "--output", dest="output",
        help="Output path, if unspecified .rtf "
        "is appended to the input path."
    )

    parser.add_argument("input", help="The input file path.")

    return parser


def main():
    """Entry point."""
    args = get_arg_parser().parse_args()

    if not args.output:
        args.output = args.input + ".rtf"

    with open(args.input) as input_file:
        with open(args.output, "w") as output_file:
            for line in input_file:
                for char in line:
                    output_file.write(rtf_replacement(char))


if __name__ == "__main__":
    main()
