#!/usr/bin/python3

"""
Converts a UTF-8 input file to an output file where all characters
have been replaced (where required and possible) with RTF escape
sequences.
"""

import logging


def rtf_replacement(utf_char):
    """Get the RTF replacement for a UTF character."""
    code_point = ord(utf_char)

    if code_point == 160:
        # No-break space (do we need this?)
        return " "

    if code_point <= 127:
        return utf_char

    if 128 <= code_point <= 255:
        return "\\'" + str(code_point)

    if 256 <= code_point <= 32767:
        return "\\uc1\\u" + str(code_point) + "*"

    if 32768 <= code_point <= 65535:
        negative_code_point = code_point - 65536
        return "\\uc1\\u-" + str(negative_code_point) + "*"

    logging.warning(
        "Warning can not replace utf-8 character %s, "
        "using '�' as a placeholder.", utf_char)
    return "�"
