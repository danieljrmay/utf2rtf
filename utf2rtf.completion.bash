# Bash Completion scriptlet for utf2rtf
#
# Author: Daniel J. R. May <daniel.may@danieljrmay.com>
# Version: 0.1

function _utf2rtf()
{
    # shellcheck disable=SC2034  # words and cword are defined by _init_completion
    local cur prev words cword
    _init_completion || return
    
    local options='-h --help -o --output'
    
    case $prev in
	utf2rtf)
	    mapfile -t COMPREPLY < <(compgen -W "$options" -- "$cur")
	    _filedir
	    return
	    ;;	
	-h|--help)
	    return
	    ;;
    esac

    if [[ $cur = -* ]]
    then
	mapfile -t COMPREPLY < <(compgen -W "$options" -- "$cur")
	return
    fi

    _filedir
}
complete -F _utf2rtf utf2rtf
