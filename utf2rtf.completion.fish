#
# Completions for the utf2rtf command
#

complete -c utf2rtf -s h -l help -f -d "Show help"
complete -c utf2rtf -s o -l output -r -F -d "Output file"
