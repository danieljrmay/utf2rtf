"""setuptools configuration for the utf2rtf package.

See: https://packaging.python.org/tutorials/packaging-projects
"""

import setuptools

with open("README.md", "r") as fh:
    LONG_DESCRIPTION = fh.read()

setuptools.setup(
    name="utf2rtf",
    version="0.1",
    author="Daniel J. R. May",
    author_email="daniel.may@danieljrmay.com",
    description="Convert Unicode characters to RTF character sequences",
    long_description=LONG_DESCRIPTION,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/danieljrmay/utf2rtf",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: OS Independent",
        "Development Status :: 3 - Alpha",
    ],
    include_package_data=False,
    entry_points={
        "console_scripts": ["utf2rtf=utf2rtf.__main__:main"],
    },
    test_suite="utf2rtf.tests.test_utf2rtf",
)
