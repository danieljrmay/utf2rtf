%global srcname utf2rtf

Name:      python-%{srcname}
Version:   0.3
Release:   1%{?dist}
Summary:   Convert unicode files to RTF

License:   GPLv3
URL:       https://gitlab.com/danieljrmay/%{srcname}
Source0:   %{url}/-/archive/%{version}/%{srcname}-%{version}.tar.bz2

BuildArch: noarch
BuildRequires: make python3-setuptools

%description
Tool to convert unicode files to RTF.


%package -n python3-%{srcname}
Summary:       Python 3 build of the utf2rtf module
BuildRequires: python3-devel
%{?python_provide:%python_provide python3-%{srcname}}

%description -n python3-%{srcname}
Python 3 module which provides the core functionality for the
utf2rtf tool.


%package -n %{srcname}
Summary:  %{summary}
Requires: python3-%{srcname} = %{version}-%{release}

%description -n %{srcname}
Tool to convert unicode files to RTF.


%prep
%autosetup -n %{srcname}-%{version}


%build
%py3_build
%make_build


%install
%py3_install
%make_install


%check
%{__python3} setup.py test


%files -n python3-%{srcname}
%doc README.md
%license LICENSE
%{python3_sitelib}/*


%files -n %{srcname}
%doc README.md
%license LICENSE
%{_bindir}/%{srcname}
%{_datadir}/bash-completion/completions/%{srcname}
%{_mandir}/man1/%{srcname}.1.gz


%changelog
* Fri Feb 18 2022 Daniel J. R. May <daniel.may@danieljrmay.com> - 0.3-1
- Update BuildRequires to include make and python3-setuptools.

* Wed Nov 18 2020 Daniel J. R. May <daniel.may@danieljrmay.com> - 0.2-1
- Fix man file syntax.

* Tue Jan 29 2019 Daniel J. R. May <daniel.may@danieljrmay.com> - 0.1-1
- Initial release.
